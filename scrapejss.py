"""
Service that uses the JSS Classic API to send metrics to Graphite.

Compatible with Python 3.6+
"""

import argparse
import pickle
import signal
import socket
import struct
import time
from logging import getLogger
from os import environ
from random import uniform
from threading import Event, Thread

import sdnotify
import requests
from requests.exceptions import ReadTimeout

import xml.etree.ElementTree as ET

LOG = getLogger()

try:
    api_url = environ["JSS_BASE_URL"]
except KeyError:
    LOG.critical("JSS_BASE_URL not found in the environment. Unable to continue.")
    exit(1)

try:
    api_username = environ["JSS_USERNAME"]
except KeyError:
    LOG.critical("JSS_USERNAME not found in the environment. Unable to continue.")
    exit(1)

try:
    api_password = environ["JSS_PASSWORD"]
except KeyError:
    LOG.critical("JSS_PASSWORD not found in the environment. Unable to continue.")
    exit(1)

# List of tuples in format (int, {"computers" or "mobile_devices"})
SEARCHES = []

CURRENT_HTML = ""
RUN_EVENT = Event()
NOTIFIER = sdnotify.SystemdNotifier()
THREADS = []

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("--computer", action='append',
                    help="A computer search to send to Graphite",
                    metavar=('name'),
                    type=int,)
parser.add_argument("--mobile", action='append',
                    help="A mobile device search to send to Graphite",
                    metavar=('name'),
                    type=int,)
parser.add_argument("-g", help="Graphite server IP or hostname", default="localhost", metavar="host")
parser.add_argument("-p", help="Pickle port to send metrics to on Graphite server", default=2004, metavar="port")
parser.add_argument("-d", help="Duration, in seconds, to wait between each update. Defaults to 900.", default=900, type=int)
parser.add_argument("--no-carbon", help="Run without a Carbon server, printing metrics to stdout.",
                    dest="no_carbon", action="store_true")
parser.epilog = "One or more `--mobile` or `--computer` arguments must be added. In the environment, set `JSS_BASE_URL` to your JSS's base URL, `JSS_USERNAME` to your username, and `JSS_PASSWORD` to your password."
ARGS = parser.parse_args()

DELAY = ARGS.d
GRAPHITE_HOST = ARGS.g
GRAPHITE_PORT = ARGS.p
NO_CARBON = ARGS.no_carbon

try:
    for computer_search in ARGS.computer:
        SEARCHES.append((computer_search, "computers"))
except TypeError:
    pass

try:
    for mobile_device_search in ARGS.mobile:
        SEARCHES.append((mobile_device_search, "mobile_devices"))
except TypeError:
    pass

if not SEARCHES:
    LOG.critical("No searches were specified. Cannot continue.")
    exit(1)

def response(pickledata):
    """Sends pickled data to Carbon.

    :param pickledata: Pickled data in `carbon's pickle protocol format
    <https://graphite.readthedocs.io/en/latest/feeding-carbon.html#the-pickle-protocol>`_
    """
    sock = None
    try:
        sock = socket.create_connection((GRAPHITE_HOST, GRAPHITE_PORT), timeout=2)
    except (ConnectionRefusedError, BlockingIOError):
        LOG.error("Connection error while connecting to Carbon.")
        LOG.error("Did you enter the correct IP and port, is the carbon cache running, is there a firewall in the way?")
        return
    except Exception as e:
        # Something else happened, stop execution
        LOG.exception(e)
        LOG.critical("Got an error while connecting to Carbon. Stopping.")
        stop_service()

    if sock:
        header = struct.pack("!L", len(pickledata))
        message = header + pickledata
        sock.sendall(message)
        sock.close()

def now():
    return int(time.time())

# Change time string to seconds
def get_sec(millis):
    return millis / 1000

def generate_carbon_data(count, search, search_type):
    """ Generates data in the Carbon pickle format.

    :param count: Number of devices in the search

    :param search: ID of search

    :param search_type: The type of search this is, "computers" or "mobile_devices"
    """

    response_list = ([])

    # Create the tuples that will be pickled and sent to Graphite
    response_list.append(("jss.{type}.{search}".format(type=search_type, search=search), (now(), count)))

    return response_list

def update_lab(run_event, base_url, duration, search, search_type, no_carbon=False):
    """The "Main code" loop for an updating thread

    Polls the JSS every duration seconds for the metrics we're interested in,
    then sends those metrics to Carbon.

    :param run_event: Threading.Event object that signals the thread to stop.
    When this event is cleared, the thread stops.

    :param base_url: The root URL of the JSS.

    :param duration: Time, in seconds, to wait between updates.

    :param search: ID of the search to get a count of devices from

    :param search_type: "mobile_devices" or "computers"

    :param no_carbon: If true, LOG.warnings metrics to stdout rather than sending them to Graphite
    """

    try:

        if search_type == "computers":
            search_url = base_url + "/JSSResource/advancedcomputersearches/id/" + str(search)
        else:
            search_url = base_url + "/JSSResource/advancedmobiledevicesearches/id/" + str(search)

        response_list = ([])

        # If something goes wrong a few times in a row, we'll stop
        tries = 0

        while run_event.is_set():
            tries += 1

            if tries > 5:
                LOG.critical("Failed to get status of %s search %s 5 or more times, exiting.", search_type, search)
                stop_service(fail=True)

            try:
                api_response = requests.get(search_url, auth=requests.auth.HTTPBasicAuth(api_username, api_password), headers={"Accept": "application/xml"})
                api_response.raise_for_status()
            except ReadTimeout:
                LOG.error("Timed out getting status of %s search %s, will retry.", search_type, search)
                continue
            except Exception as e:
                LOG.error("Error getting status of %s search %s, will retry.", search_type, search)
                LOG.exception(e)
                continue

            text = api_response.text

            try:
                root = ET.fromstring(text)
            except ET.ParseError as e:
                LOG.exception(e)
                LOG.critical(text)
                raise Exception("Failed to parse XML in %s search %s", search_type, search)

            search_item_size = root.find("{}/size".format(search_type))
            count = int(search_item_size.text)

            try:
                response_list = generate_carbon_data(count, search, search_type)
            except AttributeError:
                LOG.warning("Invalid queue data for %s search %s", search_type, search)
                stop_service()

            if no_carbon:
                LOG.warning("%s search %s", search_type, search)
                LOG.warning(pickle.loads(pickle.dumps(response_list, protocol=2)))
            else:
                response(pickle.dumps(response_list, protocol=2))

            tries = 0
            time.sleep(duration)
    except KeyboardInterrupt:
        # Stop the thread rather than printing a backtrace, the interrupt is
        # handled elsewhere.
        return
    except Exception as e:
        LOG.exception(e)
        LOG.error("Exception occured in update_search for %s search %s", search_type, search)
        RUN_EVENT.clear()

def sigterm_handler(signal, frame):
    LOG.warning("Caught SIGTERM, stopping")
    stop_service()

def stop_service(quiet=False, fail=False):
    """ Safely stops all threads before exiting

    :param quiet: True to suppress output to stdout

    :param fail: Exit with a bad status code if true
    """

    RUN_EVENT.clear()
    for thread in THREADS:
        try:
            thread.join()
        except AssertionError:
            # If we can't join the thread, it has already stopped and is not a concern.
            pass

    if not quiet:
        LOG.warning("Finished. Goodbye.")

    if fail:
        exit(2)

def check_connection(host, port):
    try:
        sock = socket.create_connection((host, port))
        sock.close
    except Exception:
        return False

    return True

def main():
    host = GRAPHITE_HOST
    port = GRAPHITE_PORT

    if not NO_CARBON:
        LOG.warning("Making sure it's possible to connect to Carbon at %s:%s", host, port)
        if not check_connection(host, port):
            LOG.warning("Error: Failed to connect to Carbon.")
            LOG.warning("Did you enter the correct IP and port, is the carbon cache running, is there a firewall in the way?")
            exit(1)
        LOG.warning("Success.")

    LOG.warning("Making sure the JSS API is available...")

    try:
        r = requests.get(api_url + "/api/")
        r.raise_for_status()
    except Exception as e:
        LOG.exception(e)
        LOG.critical("JSS API returned an error. Please see the previous output for more information.")
        exit(1)

    LOG.warning("Done. Starting update threads...")

    RUN_EVENT.set()

    for (search, type) in SEARCHES:

        LOG.warning("Starting worker for %s search %s", type, search)

        # Wait a random amount of time up to 1 second to reduce constant API load
        time.sleep(uniform(0, 1))
        thread = Thread(target=update_lab,
                          args=(RUN_EVENT, api_url, DELAY, search, type,),
                          kwargs={"no_carbon" : NO_CARBON})
        THREADS.append(thread)
        thread.start()

    signal.signal(signal.SIGTERM, sigterm_handler)

    queue_string = ""

    for (search, type) in SEARCHES:
        queue_string= ''.join([queue_string, '    ', type, ': ', str(search), '\n'])

    # A failure to contact LabStats may clear the run event before we even get here depending on the number of queues we are starting
    if RUN_EVENT.is_set():
        LOG.warning("Finished. ScrapeLabStats is collecting data for the following searches:")
        LOG.warning(queue_string)

        if NO_CARBON:
            LOG.warning("Not sending metrics to any Carbon server.")
        else:
            LOG.warning("Data is being sent to the pickle receiver at %s:%s", host, port)

        NOTIFIER.notify("READY=1")
    else:
        LOG.critical("A search thread encountered an error and failed to start.")
        stop_service(fail=True)

    try:
        while True and RUN_EVENT.is_set():
            time.sleep(2)
    except KeyboardInterrupt:
        LOG.warning("\nCaught interrupt, stopping...")
        stop_service()


if __name__ == '__main__':
    main()
