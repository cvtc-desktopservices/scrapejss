# ScrapeJSS

A script to pull the number of devices in a Jamf Pro advanced query and push them to Graphite.

## Installation

If you'd like to run or modify the script, you can install all of its dependencies with the following command run from this directory:

```
pipenv install
```

If you don't have pipenv installed, you can install it for your user with this command:

```
pip3 install pipenv --user
```

## Running the script

In the environment, set `JSS_BASE_URL` to your JSS's base URL, `JSS_USERNAME` to your username, and `JSS_PASSWORD` to your password. I know, it's bad.

The script is run with one or more searches to monitor (their ID in the JSS) along with a Graphite Pickle receiver:

```
pipenv run python3 scrapejss.py -g [graphite_host] -p [graphite_port] --mobile 206 --computer 203
```

You can run `scrapejss.py -h` for a full list of arguments, but generally:

* One or more `--mobile` or `--computer` arguments must be added. The count of devices in each search will be sent to Graphite as `jss.mobile.{report number}` or `jss.computer.{report number}`.
* A destination address and port for Graphite pickle data. If you've got Carbon running in its default configuration, you should be sending data to port 2004. The default address and port is `localhost:2004`, which you could provide with `-g localhost -p 2004`.
* An optional duration, or length of time between checks for data, may be specified. This defaults to 900 seconds.

## Running the script automatically

This repository contains an example systemd service unit file that can be used to run the script automatically. It assumes that you've created the `scrapejss` user and placed this repository at `/home/scrapejss/scrapejss/`. Values that should be replaced or added to are placed in curly braces. If your setup is different, please modify the file further.
